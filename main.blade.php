﻿<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Zentown.cl</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <meta http-equiv="Expires" content="0">

        <meta http-equiv="Last-Modified" content="0">

        <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">

        <meta http-equiv="Pragma" content="no-cache">

        <link rel="shortcut icon" href="{{{ asset('/fav.ico') }}}">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js" type="text/javascript"></script>


    {!!Html::script('plugins/timepicker/bootstrap-timepicker.min.js')!!}
    {!!Html::script('plugins/select2/select2.full.min.js')!!}

    {!!Html::script('plugins/input-mask/jquery.inputmask.js')!!}
    {!!Html::script('plugins/input-mask/jquery.inputmask.date.extensions.js')!!}
    {!!Html::script('plugins/input-mask/jquery.inputmask.extensions.js')!!}
    {!!Html::script('plugins/colorpicker/bootstrap-colorpicker.min.js')!!}

    {!!Html::style('bootstrap/css/bootstrap.min.css')!!}
    {!!Html::style('plugins/select2/select2.min.css')!!}
    {!!Html::style('plugins/bootstrap-daterangepicker/daterangepicker.css')!!}
    {!!Html::style('plugins/datepicker/css/datepicker3.css')!!}
    {!!Html::style('plugins/fullcalendar/fullcalendar.min.css')!!}
    {!!Html::style('plugins/fullcalendar/fullcalendar.print.css', ['media'=>'print'])!!}
    {!!Html::style('plugins/datatables/dataTables.bootstrap.css')!!}
    {!!Html::style('plugins/morris/morris.css')!!}
    {!!Html::style('plugins/jvectormap/jquery-jvectormap-1.2.2.css')!!}
    {!!Html::style('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')!!}
    {!!Html::style('plugins/timepicker/bootstrap-timepicker.min.css')!!}
    {!!Html::style('plugins/simplecolorpicker/jquery.simplecolorpicker.css')!!}
    {!!Html::style('dist/css/AdminLTE.css')!!}
    {!!Html::style('plugins/iCheck/all.css')!!}
    {!!Html::style('dist/css/sweetalert2.min.css')!!}
    {!!Html::style('dist/css/zentaStyle.css')!!}
    {!!Html::style('dist/css/skins/_all-skins.css')!!}
    {!!Html::style('dist/css/buttonChecked.css')!!}
    {!!Html::style('dist/css/fileinput-rtl.css')!!}
    {!!Html::style('dist/css/fileinput-rtl.min.css')!!}
    {!!Html::style('dist/css/fileinput.css')!!}
    {!!Html::style('dist/css/fileinput.min.css')!!}
    {!!Html::style('dist/css/floatingButton.css')!!}
    {!!Html::style('dist/css/bootstrap-chosen.css')!!}
    {!!Html::style('dist/css/styleZ2018.css')!!}
    {!!Html::style('dist/css/app.css')!!}

    @yield('styleHead')

    {!!Html::script('plugins/flot/jquery.flot.min.js')!!}
    {!!Html::script('plugins/flot/jquery.flot.resize.min.js')!!}
    {!!Html::script('plugins/flot/jquery.flot.pie.min.js')!!}
    {!!Html::script('plugins/flot/jquery.flot.categories.min.js')!!}
    {!!Html::script('plugins/slimScroll/jquery.slimscroll.min.js')!!}
    {!!Html::script('dist/js/demo.js')!!}
    {!!Html::script('dist/js/sweetalert2.min.js')!!}
    {!!Html::script('dist/js/fileinput.js')!!}
    {!!Html::script('dist/js/fileinput.min.js')!!}
    {!!Html::script('dist/js/plugins/piexif.min.js')!!}
    {!!Html::script('dist/js/plugins/piexif.js')!!}
    {!!Html::script('dist/js/plugins/purify.min.js')!!}
    {!!Html::script('dist/js/plugins/purify.js')!!}
    {!!Html::script('dist/js/plugins/sortable.min.js')!!}
    {!!Html::script('dist/js/plugins/sortable.js')!!}
    {!!Html::script('dist/js/jquery.fileDownload.js')!!}

    @yield('scriptHead')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <style type="text/css">
        @media (min-width : 767px) {
            .sidebar-toggle-main{
                display: none;
            }

            .logo2{
                display: none !important;
            }
        }

        @media (max-width : 399px) {
            .navbar-index{
                margin-top: -50px !important;
            }
            
        }

        @media (max-width : 767px) {
            .logo{
                display: none !important;
            }

            .logo2{
                text-align: center !important;
            }
        }

        .logo2{
            -webkit-transition: width 0.3s ease-in-out;
            -o-transition: width 0.3s ease-in-out;
            transition: width 0.3s ease-in-out;
            display: block;
            float: left;
            height: 55px;
            font-size: 18px;
            line-height: 55px;
            text-align: left;
            width: 250px;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            padding: 0 5px;
            font-weight: 300;
            overflow: hidden;
            color: white;
        }

        .logo2:hover{
            color: white;
        }
    </style>
</head>
<body class="fixed sidebar-mini sidebar-mini-expand-feature skin-yellow" style="height: auto; min-height: 100%;">
    <div class="wrapper">
        <header class="main-header">
            <div>
                <a class="logo">
                    <span>{!!Html::image('dist/img/logo_z.png', null, ['class' => 'logo-z'])!!}<b>  Zentown</b></span>
                </a>
            </div>

            <nav class="navbar navbar-static-top">
                <a href="#" class="sidebar-toggle sidebar-toggle-main" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a href="#" class="logo2">
                    <span>{!!Html::image('dist/img/logo_z.png', null, ['class' => 'logo-z'])!!}<b>  Zentown</b></span>
                </a>
                <div class="navbar-custom-menu navbar-index">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle menu-padding" data-toggle="dropdown">
                                {!!Html::image(config('services.aws.url').Auth::user()->avatar_url, null, ['onError'=>'this.onerror=null;this.src="'.url()->to('/').'/dist/img/default_avatar.jpg'.'";','class' => 'img-circle img-thumbnail widget-img navbar-avatar'])!!}
                                <span class="hidden-xs"><b>{{ Auth::user()->getFullName() }}</b></span> <i
                                        class="fa fa-caret-down" aria-hidden="true"></i>
                            </a>
                            <ul class="dropdown-menu" style="width: 293px;">
                                <li class="user-header">
                                    {!!Html::image(config('services.aws.url').Auth::user()->avatar_url, null, ['onError'=>'this.onerror=null;this.src="'.url()->to('/').'/dist/img/default_avatar.jpg'.'";','class' => 'img-circle', 'alt' => 'User Image'])!!}
                                    <p>
                                        {{ Auth::user()->getFullName() }}
                                        <small>Perfil: {{ Auth::user()->getPositionName() }}</small>
                                    </p>
                                </li>
                                <li class="btn-group" >
                                    <div class="btn btn-default">
                                        <a href="{{ route('profile.index') }}" style="color: black;">Mi Perfil</a>
                                        </div>
                                    <div class="btn btn-default">
                                        <a href="{{ route('firm.index') }}" style="color: black;">Generar Firmas</a>
                                    </div>
                                    <div class="btn btn-default">
                                        <a href="" style="color: black;" id="btn-logout">
                                            Cerrar Sesión
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar" >
            <section class="sidebar">
                <ul class="sidebar-menu" >
                    @can('menu.home')
                    <li class="treeview">
                        <a href="{{ route('home') }}">
                            <i class="fa fa-home"></i> <span>Inicio</span>
                            <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                          </span>
                        </a>
                    </li>
                    @endcan

                    @can('menu.calendar')
                    <li class="treeview">
                        <a href="{{ route('wellness.calendar') }}">
                            <i class="fa fa-calendar"></i> <span>Eventos</span>
                            <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                          </span>
                        </a>
                    </li>
                    @endcan
                    @can('menu.administration')
                        <li class="treeview">
                            <a href="{{ route('slack.index')}}">
                                <i class="fa fa-slack"></i> <span>Mensajes a Slack</span>
                                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="{{ route('administration.zentin.post')}}">
                                <i class="fa fa-comments-o"></i> <span>Mensajes de Zentin</span>
                                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                            </span>
                            </a>
                        </li>
                    @endcan
                    @can('menu.enterprise')
                    <li class="treeview">
                        <a href="{{ route('administration.zentin.post')}}">
                            <i class="fa fa-thumbs-up"></i> <span>Referencias</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                          <ul class="treeview-menu">

                            <li class="{{ $activePage=='referral'?'active treeview':''}}"><a
                                        href="{{ route('referral.myreferences') }}"><i class="fa fa-circle-o"></i>Mis Referenciados</a></li>

                        @can('menu.administration')

                            <li class="{{ $activePage=='referral'?'active treeview':''}}"><a
                                        href="{{ route('referral.admin') }}"><i class="fa fa-circle-o"></i>Administrador</a></li>

                            <li class="{{ $activePage=='referral'?'active treeview':''}}"><a
                                        href="{{ route('referral.rules') }}"><i class="fa fa-circle-o"></i>Listar regla</a></li>

                            <li class="{{ $activePage=='referral'?'active treeview':''}}"><a
                                        href="{{ route('referral.viewGraphics') }}"><i class="fa fa-circle-o"></i>Graficas</a></li>
                                        <li class="{{ $activePage=='referral'?'active treeview':''}}"><a
                                        href="{{ route('referral.historyReferral') }}"><i class="fa fa-circle-o"></i>Historico</a></li>

                            <li class="{{ $activePage=='referral'?'active treeview':''}}"><a
                                        href="{{ route('referral.referidos') }}"><i class="fa fa-circle-o"></i>Referidos Test</a></li>



                        @endcan
                        </ul>
                    </li>
                    @endcan
                    @can('menu.enterprise')
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-industry"></i> <span>Empresa</span>
                            <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                          </span>
                        </a>
                        <ul class="treeview-menu">
                            @can('menu.organization.chart')
                            <li class="{{ $activePage=='organizationChart'?'active treeview':''}}"><a
                                        href="{{ route('organizationChart.index') }}"><i class="fa fa-circle-o"></i>Organigrama</a></li>
                            @endcan
                            @can('menu.knowledge.capsule')
                            <li class="{{ $activePage=='knowledgeCapsule'?'active treeview':''}}"><a
                                        href="{{ route('knowledgeCapsule.index') }}"><i class="fa fa-circle-o"></i>Cápsula de Conocimiento</a></li>
                            @endcan
                        </ul>
                    </li>
                    @endcan

                    @can('menu.administration')
                        <li class="{{ $activeTreeview=='maintainers'?'treeview active menu-open':'treeview'}}">
                            <a href="#">
                                <i class="fa fa- fa-database"></i> <span>Mantenedores</span>
                                <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                            </a>
                            <ul class="treeview-menu">
                                @can('menu.list.position')
                                    <li class="{{ $activePage=='position'?'active treeview':''}}"><a
                                                href="{{ route('position.index') }}"><i class="fa fa-circle-o"></i>Listado de Cargos</a></li>
                                @endcan
                                   @can('menu.add.position')
                                    <li class="{{ $activePage=='position'?'active treeview':''}}"><a
                                                href="{{ route('position.create') }}"><i class="fa fa-circle-o"></i>Agregar Cargos</a></li>
                                @endcan
                                <li class="{{ $activePage=='poll'?'active':''}}"><a href="{{ route('maintainers.polls') }}"><i class="fa fa-circle-o"></i>Encuestas</a></li>
                                <li class="{{ $activePage=='afp'?'active':''}}"><a href="{{ route('maintainers.afp') }}"><i class="fa fa-circle-o"></i>AFP</a></li>
                                <li class="{{ $activePage=='asset'?'active':''}}"><a href="{{ route('maintainers.asset') }}"><i class="fa fa-circle-o"></i>Bonos</a></li>
                                <li class="{{ $activePage=='bank'?'active':'bank'}}"><a href="{{route('maintainers.bank')}}"><i class="fa fa-circle-o"></i>Bancos</a></li>
                                <li class="{{ $activePage=='branch'?'active':''}}"><a href="{{ route('maintainers.branch') }}"><i class="fa fa-circle-o"></i>Oficinas</a></li>
                                <li class="{{ $activePage=='communes'?'active':''}}"><a href="{{route('maintainers.communes')}}"><i class="fa fa-circle-o"></i>Comunas</a></li>
                                <li class="{{ $activePage=='company'?'active':''}}"><a href="{{ route('maintainers.company') }}"><i class="fa fa-circle-o"></i>Compañias</a></li>
                                <li class="{{ $activePage=='discount'?'active':''}}"><a href="{{route('maintainers.discounts')}}"><i class="fa fa-circle-o"></i>Descuentos</a></li>
                                <li class="{{ $activePage=='isapre'?'active':''}}"><a href="{{route('maintainers.isapre')}}"><i class="fa fa-circle-o"></i>Isapre</a></li>
                                <li class="{{ $activePage=='province'?'active':''}}"><a href="{{route('maintainers.province')}}"><i class="fa fa-circle-o"></i>Provincias</a></li>
                                <li class="{{ $activePage=='question'?'active':''}}"><a href="{{ route('maintainers.question') }}"><i class="fa fa-circle-o"></i>Preguntas</a></li>
                                <li class="{{ $activePage=='region'?'active':''}}"><a href="{{ route('maintainers.region') }}"><i class="fa fa-circle-o"></i>Regiones</a></li>

								<li class="{{ $activePage=='contract'?'active':''}}"><a href="{{ route('maintainers.contract') }}"><i class="fa fa-circle-o"></i>Contratos</a></li>
	
                                <li class="{{ $activePage=='salary'?'active':''}}"><a href="{{ route('maintainers.salary') }}"><i class="fa fa-circle-o"></i>Salarios por Cargo</a></li>
				<li class="{{ $activePage=='salaryHistory'?'active':''}}"><a href="{{ route('maintainers.salaryHistory') }}"><i class="fa fa-circle-o"></i>Historial pago</a></li>

                                {{--ACHIEVEMENTS--}}
                                <li class="{{ $activePage=='achievementType'?'active':''}}"><a href="{{ route('maintainers.achievementType') }}"><i class="fa fa-circle-o"></i>Logros</a></li>
                
                            </ul>

                        </li>

                    @endcan

                    @can('menu.view.collaborator')
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-file-text-o"></i> <span>Colaboradores</span>
                            <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                          </span>
                        </a>
                        <ul class="treeview-menu">
                            @can('menu.contract')
                            <li class="{{ $activePage=='contract'?'active treeview':''}}"><a href="{{ route('contract') }}"><i
                                            class="fa fa-circle-o"></i>Contratar</a></li>
                            @endcan
                            @can('menu.list.collaborator')
                            <li class="{{ $activePage=='collaborators'?'active treeview':''}}"><a
                                        href="{{ route('collaborators.collaboratorsFilter') }}"><i class="fa fa-circle-o"></i>Listar</a></li>
                            @endcan

                            @can('menu.view.salary')
                                    <li class="{{ $activePage=='collaborators'?'active treeview':''}}"><a
                                                href="{{ route('collaborators.collaboratorsFilterSA') }}"><i class="fa fa-circle-o"></i>Solicitud de Aumento</a></li>
                            @endcan
                            @can('menu.assigned.leadership')
                            <li class="{{ $activePage=='collaborators'?'active treeview':''}}"><a
                                        href="{{ route('coordinationFilter.filter') }}"><i class="fa fa-circle-o"></i>Asignar Líder</a></li>
                            @endcan
                            @can('menu.administration')
                                <li class="{{ $activePage=='collaborators'?'active treeview':''}}"><a href="{{ route('download.ETL') }}"><i
                                                class="fa fa-circle-o"></i>Descargar ETL</a></li>
                            @endcan
                        </ul>
                    </li>
                    @endcan

                    @can('menu.remuneration')
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-dollar"></i> <span>Liquidaciones De Sueldo</span>
                            <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                          </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="{{ $activePage=='contract'?'active treeview':''}}"><a href="{{ route('remuneration.index') }}"><i
                                            class="fa fa-circle-o"></i>Nómina</a></li>
                            <li class="{{ $activePage=='contract'?'active treeview':''}}"><a href="{{ route('remuneration.indicators') }}"><i
                                            class="fa fa-circle-o"></i>Indicadores Económicos</a></li>
                            @can('menu.view.salary')
                                <li class="{{ $activePage=='contract'?'active treeview':''}}"><a href="{{ route('collaborators.view') }}"><i
                                            class="fa fa-circle-o"></i>Solicitudes Aumento de Sueldo</a></li>
                            @endcan
                        </ul>
                    </li>
                    @endcan

                    @can('menu.my.documents')
                    <li class="treeview">
                        <a href="{{ route('myDocuments.index',['id'=>Auth::user()->personErp()->id]) }}">
                            <i class="fa fa-file-text-o"></i> <span>Mis Documentos</span>
                            <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                          </span>
                        </a>
                    </li>
                    @endcan

                    @can('menu.list.holidays')
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-plane"></i> <span>Vacaciones</span>
                                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                              </span>
                            </a>
                            <ul class="treeview-menu">
                                @can('menu.sub.my.holidays')
                                <li class="{{ $activePage=='request'?'active treeview':''}}">
                                    <a href="{{ route('vacations.history', ['id'=>Auth::user()->personErp()->id]) }}"><i class="fa fa-circle-o"></i>Mis Vacaciones</a>
                                </li>
                                @endcan
                                @can('menu.list.holidays')
                                <li class="{{ $activePage=='request'?'active treeview':''}}">
                                    <a href="{{ route('holidays.holidaysListByLeader') }}"><i class="fa fa-circle-o"></i>Listado</a>
                                </li>
                                @endcan
								@can('menu.administration')
                                <li class="{{ $activePage=='request'?'active treeview':''}}"><a
                                            href="{{ route('request.allHolidays') }}"><i class="fa fa-circle-o"></i>Listado de solicitudes</a></li>
                                @endcan
                            </ul>
                        </li>
                    @else
                        <li class="treeview">
                            <a href="{{ route('vacations.history', ['id'=>Auth::user()->personErp()->id]) }}">
                                <i class="fa fa-plane"></i> <span>Mis Vacaciones</span>
                                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                              </span>
                            </a>
                        </li>
                    @endcan

                    @can('menu.administration')
                        
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-balance-scale"></i><span> Datos Bancarios</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                            </a>

                            <ul class="treeview-menu">
                                     <li class="{{ $activePage=='request'?'active treeview':''}}"><a
                                             href="{{ route('banks.view',['id'=>Auth::user()->personErp()->id]) }}"><i class="fa fa-circle-o"></i>Solicitud Cambio de cuenta</a>
                                 </li> 
                            </ul>

                        </li>
                    @endcan

                    @can('menu.administration')
                    <li class="treeview">
                         <a href="#">
                             <i class="fa fa-calendar-plus-o"></i> <span>Mis Licencias</span>
                             <span class="pull-right-container">
                             <i class="fa fa-angle-left pull-right"></i>
                           </span>
                         </a>
                         <ul class="treeview-menu">
                             @can('menu.sub.my.holidays')
                             <li class="{{ $activePage=='request'?'active treeview':''}}"><a
                                         href="{{ route('licences.showMe',['id'=>Auth::user()->personErp()->id]) }}"><i class="fa fa-circle-o"></i>Estado Licencia</a>
                             </li>
                             @endcan
                             @can('menu.list.holidays')
                             <li class="{{ $activePage=='request'?'active treeview':''}}"><a
                                         href="{{ route('licences.uploadLicences',['id'=>Auth::user()->personErp()->id]) }}"><i class="fa fa-circle-o"></i>Agregar Licencia</a></li>
                             @endcan
                         </ul>
                    </li>
                    @endcan

                    @can('menu.my.timeline')
                        <li class="treeview">
                            <a href="{{Route('profile.my.timeline',['type'=>'my','id'=>Auth::user()->personErp()->id])}}">
                                <i class="fa fa-line-chart"></i> <span>Mi Evolución</span>
                                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                              </span>
                            </a>
                        </li>
                    @endcan

                    @can('menu.indicators')
                    <li class="treeview">
                        <a href="{{ route('indicators.index') }}">
                            <i class="fa fa-bar-chart"></i> <span>Indicadores</span>
                            <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                          </span>
                        </a>
                    </li>
                    @endcan

                    @can('menu.administration')
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-file-text-o"></i> <span>Administración</span>
                            <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                          </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="{{ $activePage=='users'?'active treeview':''}}"><a href="{{ route('administration.users') }}"><i
                                            class="fa fa-circle-o"></i>Usuarios</a></li>
                            <li class="{{ $activePage=='roles'?'active treeview':''}}"><a
                                        href="{{ route('administration.roles') }}"><i class="fa fa-circle-o"></i>Perfiles</a></li>
                            <li class="{{ $activePage=='chartRoles'?'active treeview':''}}"><a
                                        href="{{ route('administration.chartRoles') }}"><i class="fa fa-circle-o"></i>Distribución de Roles</a></li>
                            <li class="{{ $activePage=='permissions'?'active treeview':''}}"><a
                                        href="{{ route('administration.permissions') }}"><i class="fa fa-circle-o"></i>Permisos</a></li>
                        </ul>
                    </li>
                    @endcan
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-file-text-o"></i> <span>Ev Desempeño</span>
                            <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                          </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="{{ $activePage=='request'?'active treeview':''}}"><a href="{{ route('evaluations.index') }}"><i
                                        class="fa fa-circle-o"></i>Evaluaciones</a></li>
                            @can('menu.performance.evaluation')
                            <li class="{{ $activePage=='request'?'active treeview':''}}"><a href="{{ route('evaluations.maintainer.index') }}">
                                    <i class="fa fa-circle-o"></i>Mantener Evaluaciones</a>
                            </li>  
                            @endcan                          
                        </ul>
                    </li>
                </ul>
            </section>
        </aside>

        <div class="content-wrapper">
            @yield('content')
            
            @if(Request::url() != route('start'))
                <!-- javascript:history.back() -->
                <div id="container-floating">
                    <a id="floating-button" class="edit-Rem" href="javascript:history.back()">
                        <p class="plus"><i class="fa fa-undo" aria-hidden="true"></i></p>
                        <p class="edit">Volver</p>
                    </a>
                </div>
            @endif
        </div>
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Versión</b> 1.0
            </div>
            <strong>Copyright &copy; 2017 <a href="http://zentagroup.com">Zenta Solutions</a>.</strong> Todos los derechos
            reservados.
        </footer>
        <div class="modal-gears"><!-- Place at bottom of page --></div>
        <div class="control-sidebar-bg"></div>
    </div>

    {!!Html::script('plugins/jQuery/jquery-2.2.3.min.js')!!}
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    {!!Html::script('bootstrap/js/bootstrap.min.js')!!}
    {!!Html::script('plugins/datatables/jquery.dataTables.min.js')!!}
    {!!Html::script('plugins/datatables/dataTables.bootstrap.min.js')!!}
    {!!Html::script('plugins/morris/morris.min.js')!!}
    {!!Html::script('plugins/sparkline/jquery.sparkline.min.js')!!}
    {!!Html::script('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')!!}
    {!!Html::script('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')!!}
    {!!Html::script('plugins/knob/jquery.knob.js')!!}
    {!!Html::script('plugins/bootstrap-daterangepicker/daterangepicker.js')!!}
    {!!Html::script('plugins/datepicker/js/bootstrap-datepicker.js')!!}
    {!!Html::script('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')!!}
    {!!Html::script('plugins/slimScroll/jquery.slimscroll.min.js')!!}
    {!!Html::script('plugins/fastclick/fastclick.js')!!}
    {!!Html::script('plugins/iCheck/icheck.min.js')!!}
    {!!Html::script('dist/js/app.min.js')!!}
    {!!Html::script('plugins/fullcalendar/fullcalendar.min.js')!!}
    {!!Html::script('plugins/fullcalendar/locale-all.js')!!}
    {!!Html::script('plugins/simplecolorpicker/jquery.simplecolorpicker.js')!!}
    {!!Html::script('plugins/chartjs/Chart.min.js')!!}
    {!!Html::script('dist/js/chosen.jquery.js')!!}
    <!--
        Script del chat app.purechat.com
    -->
    <script type='text/javascript' data-cfasync='false'>
        window.purechatApi =
            {
                l: [],
                t: [],
                on: function ()
                {
                    this.l.push(arguments);
                }
            };
        (function () {
            var done = false;
            var script = document.createElement('script');
            script.async = true;
            script.type = 'text/javascript';
            script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';
            document.getElementsByTagName('HEAD').item(0).appendChild(script);
            script.onreadystatechange = script.onload = function (e)
            {
                if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete'))
                {
                    var w = new PCWidget({c: '832c5e5e-91d3-4a99-b55e-2fedf65c4611', f: true }); done = true;
                }
            };
        })();
        purechatApi.on('chatbox:ready', function () {
            purechatApi.set('visitor.name', '{{Auth::user()->getFullName()}}');
            purechatApi.set('visitor.email', '{{Auth::user()->email}}');
        });
    </script>
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script>
        function relocate_home(route) {
            location.href = route;
        }
    </script>

    <script>
        $(function () {
            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            });
        });
    </script>

    <script>
        $('.edit-Rem').on('click', function () {
            $body = $("body");
            $body.addClass("loading");
        });

        $('#btn-logout').on('click', function(e){
            e.preventDefault();
            form = $('#logout-form');
            swal({
                title: 'Cerrar Sesión',
                text: 'Está seguro de cerrar sesión ?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<i class="fa fa-thumbs-up"></i> Salir!',
                cancelButtonText:  '<i class="fa fa-thumbs-down"></i> Cancelar',
            }).then(function () {
                form.submit();
            })
        })

    </script>

    @yield('scriptFooter')
</body>
</html>
